package vueB;

import java.util.ArrayList;
import java.util.Scanner;


public class VueConsole 
{
	
	private Scanner input;
	public String lastInput;

	public VueConsole(){
		lastInput = "";
		input = new Scanner(System.in);
		input.useDelimiter("\r\n");
	}

	public String demandeInput(){
		lastInput = input.nextLine();
		return lastInput;
	}
	
	public void afficheLigneSimple(String ligne){
		System.out.println(ligne);
	}
	
	public void afficheLignePasseLigne(String ligne){
		System.out.println(ligne + "\n");
	}
	
	public void afficheLigneSansRetour(String ligne){
		System.out.print(ligne);
	}
	
  public void afficheMenu() {
    afficheLigneSimple("(1) Nouvelle partie");
		afficheLigneSimple("(2) Quitter");
		afficheLigneSansRetour("Votre choix >>> ");
		afficheLigneSimple("");
  }
  
	public String getLastInput(){
		return lastInput;
	}
	
  public void affichePlaques(ArrayList<Integer> arrayList) { // TO DO
    for(Integer integ : arrayList)
      System.out.print("" + integ + ", ");
  }

  public void afficheSolution(ArrayList<String> listeSolution) {// TO DO
    for(String s : listeSolution)
      System.out.println(s);
  }

  
}
