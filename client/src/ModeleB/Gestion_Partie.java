
package ModeleB;

import State_ModeleB_GPartie.GP_TrtInitial;
import State_ModeleB_GPartie.GP_State;
import java.util.ArrayList;

/**
 *
 * @author Pierre
 */
public class Gestion_Partie extends GP_Observable{ 

  private GP_State myState;
  private Solution solution;
  //private GP_CareTakerRound tourGardien; //for IG only
  //private GP_CareTaker gardien;          //for IG only
  
  public Gestion_Partie(){
    initialize();
    myState = new GP_TrtInitial(this);
  }

  private void initialize() {
     //tourGardien = new GP_CareTakerRound();  //for IG only
     //gardien = new GP_CareTaker();           //for IG only
     solution = new Solution();
  }
  
  public void start(){
    this.getMyState().start();
  }
  
  public void traitementChoix(Object o) {
    this.getMyState().traitementChoix(o);
  }

  public void setMyState(GP_State state) {
    this.myState = state;
  }
  
  public String getMyStateString(){
    return getMyState().getStateString();
  }
  
  public String getStringOpeArithm(String stateString){
    return getMyState().getStringOpeArithm(stateString);
  }
 
  public ArrayList<Integer> getPlaquesdispo(){
    return getMyState().getPlaquesdispo();
  }
  
  /**
   * @return the myState
   */
  public GP_State getMyState() {
    return myState;
  }
  
  /**
   * @return the solution
   */
  public Solution getSolution() {
    return solution;
  }
  
  public ArrayList<String> getListeSolution() {
     return getSolution().stringlistOpeAri();
  }
  
//Traitement des réponses du serveur:
  
  public void trtReponseServerNvJeu(Integer lebut, ArrayList<Integer> plaquesDispo) {
    myState.trtReponseServerNvJeu(lebut, plaquesDispo);
  }

  public void trtReponseServerAnnonceDelaiOk() {
    myState.trtReponseServerAnnonceDelaiOk();
  }
  
  public void trtReponseServerSolutionOk(int point){
    myState.trtReponseServerSolutionOk(point);
  }
  
}