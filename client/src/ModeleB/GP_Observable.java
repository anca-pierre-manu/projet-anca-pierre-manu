/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeleB;

import controllerB.GP_Observer;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Pierre
 */
abstract class GP_Observable {
  
  private List<GP_Observer> gpObs = new LinkedList<>();
  
  public void subscribe(GP_Observer o){
    gpObs.add(o);
  }
  
  public void unsubcribe(GP_Observer o){
    gpObs.remove(o);
  }
  
  public void notifyObservers(String state,String subjet,Object o){
    for(GP_Observer ob : gpObs){
      ob.update(state, subjet, o);
    }
  }
  
}
