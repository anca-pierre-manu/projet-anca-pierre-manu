/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeleB;

/**
 *
 * @author Pierre
 */
public class  ErreurOrMessage { 
  
  public final static String eMenu = "Choix menu erroné ! Introduire un nombre entier égal à un ou deux.";//mode console only
  public final static String eAnnonce = "Choix annonce erroné ! Introduire un nombre entier entre 100 et 999.";
  public final static String ePlaque = "Choix plaque erroné ! Introduire une plaque disponible.";
  public final static String eOperation = "Choix opérateur arithmetique erroné ! Choisir dans (/,*,-,+).";
  public final static String mWin = "Vous avez gagné: ";
  public final static String eResultat = "Resultat erroné ! GAME OVER !";
  public final static String eMissPlaques ="Il ne vous reste plus assez de plaques pour atteindre votre annonce ! GAME OVER !";
  public final static String eCalculUn = "Un résultat négatif ne peut être possible ! Modifier votre choix !";
  public final static String eCalculDeux = "On ne peut diviser par zéro ! Modifier votre choix !";
  public final static String eCalculTrois = "La division doit être entière ! Modifier votre choix !";
  public final static String eCalcul = "Modifier votre choix !";
  
  public static String getEcalcul(int j){
    switch (j)
		{
			case -1:
				return eCalculUn;
			case -2:
				return eCalculDeux;
      case -3:
				return eCalculTrois;
      default:
        return eCalcul;
    }
  }
}
