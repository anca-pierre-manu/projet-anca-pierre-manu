/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeleB;

import java.util.ArrayList;

/**
 *
 * @author Pierre
 */
public class Solution {
  
  private Integer annonce = -1;
  private ArrayList<Operation_Arithmetique> listeOpeArithm = new ArrayList<>();
  
  public boolean verifChoixAnnonce(int i){
    if( (i>=100) && (i<=999) ){
      this.annonce = i;
      return true;
    }
    else
      return false;
  }
  
  public void addListeOpeArithm(Operation_Arithmetique opeArithm){
    getListeOpeArithm().add(opeArithm);
  }
  
  public int getAnnonce() {
    return annonce;
  }

  public Operation_Arithmetique getAndRemoveLastOpeAr(){
    return getListeOpeArithm().remove(getListeOpeArithm().size()-1);
  }
  
  public boolean isListOpeAriEmpty(){
    if(getListeOpeArithm().isEmpty())
      return true;
    else
      return false;
  }
  
  public ArrayList<String> stringlistOpeAri(){
    ArrayList<String> sl = new ArrayList<>();
    for(Operation_Arithmetique opAr: getListeOpeArithm() ){
      sl.add(opAr.toString());
    }
    return sl;
  }

  /**
   * @return the listeOpeArithm
   */
  public ArrayList<Operation_Arithmetique> getListeOpeArithm() {
    return listeOpeArithm;
  }
}