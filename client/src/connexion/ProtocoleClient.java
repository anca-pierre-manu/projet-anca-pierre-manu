package connexion;

import ModeleB.Operation_Arithmetique;
import common.Codes;
import common.Message;
import common.Protocole;
import controllerB.CommCtrl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class ProtocoleClient extends Protocole {
	private String ip;
	private int port;
	private CommCtrl controleurComm;

	public ProtocoleClient(String ipAddr, int port, CommCtrl controleurComm) {
		this.ip = ipAddr;
		this.port = port;
		this.controleurComm = controleurComm;
		if (connexionTCP() == Codes.CONN_KO) {
			System.out.println("Impossible de se connecter au serveur! ("
					+ ipAddr + ":" + port + ")");
			System.exit(-1);
		} else {
			System.out.println("Connection reussie avec le serveur.");
      
		}
	}

	public Codes connexionTCP() {
		try {
			socket = new Socket(ip, port);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			return Codes.CONN_OK;
		} catch (UnknownHostException e) {
			return Codes.CONN_KO;
		} catch (IOException e) {
			return Codes.CONN_KO;
		}
	}

	public Codes connexionGameServer() {
		Message envoi = new Message(Codes.MESSAGE_ASK_CONNECTION,
				"demandeConnection");

		write(envoi);
		try {
			Message reception = (Message) in.readObject();
			if (reception instanceof Message)
      {
        envoiAck();
				return Codes.CONN_OK;
      }
		} catch (ClassNotFoundException | IOException e) {
			return Codes.CONN_KO;
		}
		return Codes.CONN_KO;
	}

	public void disconnect() {
		Message envoi = new Message(Codes.MESSAGE_DISCONNECT, "Deconnection client.");
		
		try {
			write(envoi);
			socket.close();
		} 
		catch (IOException e) {
			System.out.println("La connection avec le serveur n'est plus disponible.");
		}
	}

	public Message attendreMessage() {
		try {
			lastReceivedPacket = (Message)in.readObject();
			envoiAck();
			return lastReceivedPacket;
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Codes envoiPaquetAuServeur(Message paquet) {
		write(paquet);
    
		try {
			Message ackReception = (Message) in.readObject();
			if (ackReception.getCodeMessage() == Codes.MESSAGE_ACK)
				return Codes.CONN_OK;
			return Codes.CONN_KO;
		} catch (ClassNotFoundException | IOException e) {
			return Codes.CONN_KO;
		}
	}
	
  public Codes envoiPaquetAuServeurSansAck(Message answer)
  {
    if(answer == null) return Codes.MESSAGE_NOT_OK;
    write(answer);
    return Codes.MESSAGE_OK;
  }
  
	private void envoiAck()
	{
		Message ack = new Message(Codes.MESSAGE_ACK, "Ack");
		envoiPaquetAuServeurSansAck(ack);
	}
	
	private boolean attendreAck()
	{
		Message retour = attendreMessage();
		if(retour.getCodeMessage() == Codes.MESSAGE_ACK)
			return true;
		return false;
	}
	
	// M�thodes de traitement des demandes
	
	public void trtDemandeNvJeu() {
		Message demandeNvJeu = new Message(Codes.MESSAGE_ASK_NEWGAME, "Demande Nouveau Jeu");
		Codes codeRetour = envoiPaquetAuServeur(demandeNvJeu);
		
		// Traiter les eventuelles erreurs de retour
		if(codeRetour != Codes.CONN_KO)
		{
			// Attendre le message avec les infos du jeu
			Message paquetContenantNouveauJeu = attendreMessage();
			
			if(paquetContenantNouveauJeu.getCodeMessage() == Codes.MESSAGE_SEND_NEWGAME)
			{
				// envoiAck(); // Attendre message envoie automatiquement un ack si la r�ponse est bien re�ue
				Integer leBut = paquetContenantNouveauJeu.getBut();
				ArrayList<Integer> plaques = (ArrayList<Integer>) paquetContenantNouveauJeu.getPlaques();
				controleurComm.reponseServerNvJeuOk(leBut, plaques);
			}
			else if(paquetContenantNouveauJeu.getCodeMessage() == Codes.MESSAGE_SEND_ERROR_ANSWER)
			{
				//Sinon c'est un message d'erreur
				
				controleurComm.reponseErreurClient("Probleme a la creation du jeu", true, true);
			}
		}
		else
		{
			//TODO check
			controleurComm.reponseErreurClient("Erreur interne au niveau du serveur. (trtDemandeNvJeu)", true, false);
		}
	}

	public void trtEnvoiAnnonce(Integer annonce) {
		Message paquetAnnonce = new Message(Codes.MESSAGE_SEND_ANNONCE, "Envoi Annonce", annonce);
		Codes codeRetour = envoiPaquetAuServeur(paquetAnnonce);
		
		if(codeRetour != Codes.CONN_KO)
		{
			Message paquetContenantReponseAnnonce = attendreMessage();
			
			if(paquetContenantReponseAnnonce.getCodeMessage() == Codes.MESSAGE_ANNONCE_OK)
			{
				controleurComm.reponseServerAnnonceDelaiOk();
			}
			else if(paquetContenantReponseAnnonce.getCodeMessage() == Codes.MESSAGE_HORS_DELAI_A)
			{
				if(paquetContenantReponseAnnonce.getErreurAcceptee())
					controleurComm.reponseErreurClient("HorsDelaiA", true, true);
				else
					controleurComm.reponseErreurClient("HorsDelaiA", true, false);
			}
		}
		else
		{
			controleurComm.reponseErreurClient("Erreur interne au niveau du serveur. (trtEnvoiAnnonce)", true, false);
		}
	}

	public void trtEnvoiSolution(ArrayList<Operation_Arithmetique> operations)
	{
		Message paquetSolution = new Message(Codes.MESSAGE_SEND_SOLUTION, "Envoi Solution", operations);
		Codes codeRetour = envoiPaquetAuServeur(paquetSolution);
		
		if(codeRetour != Codes.CONN_KO)
		{
			Message paquetContenantReponseSolution = attendreMessage();
			
			if(paquetContenantReponseSolution.getCodeMessage() == Codes.MESSAGE_SEND_SOLUTION)
			{
				Integer points = paquetContenantReponseSolution.getScoreFinalJoueur();
				controleurComm.reponseServerSolutionOk(points);
			}
			else if(paquetContenantReponseSolution.getCodeMessage() == Codes.MESSAGE_HORS_DELAI_E)
			{
				if(paquetContenantReponseSolution.getErreurAcceptee())
					controleurComm.reponseErreurClient("HorsDelaiE", true, true);
				else
					controleurComm.reponseErreurClient("HorsDelaiE", true, false);
			}
		}
		else
		{
			controleurComm.reponseErreurClient("Erreur interne au niveau du serveur. (trtEnvoiSolution)", true, false);
		}
	}

	public void trtEnvoiErreur(String erreur)
	{
		Message paquetErreur = null;
		switch(erreur)
		{
		case "HorsDelaiA": // Annonce hors d�lai
			paquetErreur = new Message(Codes.MESSAGE_HORS_DELAI_A, "HorsDelaiA", erreur);
			break;
			
		case "HorsDelayE": // Encodage (solution) hors d�lai
			paquetErreur = new Message(Codes.MESSAGE_HORS_DELAI_E, "HorsDelaiE", erreur);
			
		case "eMissPlaque": // Choix de plaque erron�
			paquetErreur = new Message(Codes.MESSAGE_EMISSPLAQUE, "Miss plaque", erreur);
			break;
			
		case "eResultat": // R�sultat final != annonce
			paquetErreur = new Message(Codes.MESSAGE_ERESULTAT, "Miss resultat", erreur);
			break;
		}
		
		envoiPaquetAuServeur(paquetErreur);
	}
}
