package controllerB;

import ModeleB.Gestion_Partie;
import java.util.ArrayList;

import vueB.VueConsole;

public class CtrlPartie implements GP_Observer
{
	private CtrlJeu ctrlJeu;
	private VueConsole console;
	private Gestion_Partie partie;
	private CommCtrl connexionReseau;

	Integer annonce;
	Integer resultat;

	private boolean firstAskA = true;
	private long startTimeA;
	private long endTimeA;
	private long deltaA = 10000;

	private boolean firstAskE = true;
	private long startTimeE;
	private long endTimeE;
	private long deltaE = 60000;

	public CtrlPartie(CtrlJeu ctrlJeu, VueConsole console, String ip, int port)
	{
		this.ctrlJeu = ctrlJeu;
		this.console = console;
		partie = new Gestion_Partie();
		connexionReseau = new CommCtrl(ip, port, this, partie);
		liaisonCtrlToPartie();
		demarrerPartie();
	}

	// _____________________________________________________________________________________

	private void liaisonCtrlToPartie()
	{
		partie.subscribe(this);// console dans le cas IG <=> interface graphique
		partie.subscribe(connexionReseau);
	}

	private void demarrerPartie()
	{
		partie.start();
	}

	// UPDATE:--------------------------------------------------------------------------------
	
	@Override
	public void update(String state, String subject, Object o)
	{
		try
		{
			switch (state)
			{
			case "TrtInitial":
				if (subject.compareTo("but") == 0)
					console.afficheLigneSimple(((Integer) o).toString());
				if (subject.compareTo("plaquesDispo") == 0)
					console.affichePlaques((ArrayList<Integer>) o);// TODO dans
																	// VueConsole
				break;
			case "TrtAnnonce":
				if (subject.compareTo("nc") == 0)// dans le cas IG on réaffiche
													// les plaques
					demandeAnnonce();
				if (subject.compareTo("annonce") == 0) // annonce acceptée par
														// le server !
					console.afficheLigneSimple(((Integer) o).toString());
				if (subject.compareTo("eAnnonce") == 0)
				{
					console.afficheLigneSimple((String) o);
					demandeAnnonce();
				}
				break;
			case "TrtPlaqueUn":
				if (subject.compareTo("nc") == 0)
				{
					if (firstAskE)
					{
						startTimeE = System.currentTimeMillis();
						firstAskE = false;
					}
					demandePlaque();
				}
				if (subject.compareTo("ePlaque") == 0)
				{
					console.afficheLigneSimple((String) o);
					demandePlaque();
				}
				break;
			case "TrtOpeFonda":
				if (subject.compareTo("nc") == 0)
					demandeOperation();
				if (subject.compareTo("eOperation") == 0)
				{
					console.afficheLigneSimple((String) o);
					demandeOperation();
				}
				break;
			case "TrtPlaqueDeux":
				if (subject.compareTo("nc") == 0)
					demandePlaque();
				if (subject.compareTo("ePlaque") == 0)
				{
					console.afficheLigneSimple((String) o);
					demandePlaque();
				}
				break;
			case "TrtResultat":
				if (subject.compareTo("nc") == 0)
					demandeResultat();
				if (subject.compareTo("fullOpeAri") == 0)
					console.afficheSolution(partie.getListeSolution());
				if (subject.compareTo("mWin") == 0)
					console.afficheLigneSimple((String) o);
				if (subject.compareTo("eMissPlaques") == 0)
					trtErreurFatale("eMissPlaques");
				if (subject.compareTo("eResultat") == 0)
					trtErreurFatale("eResultat");
			}
		} catch (NullPointerException e)
		{
			e.printStackTrace();
		}
	}

	// methode d'interaction console/partie:
	// -------------------------------------

	private void demandeAnnonce()
	{
		if (firstAskA)
		{
			startTimeA = System.currentTimeMillis();
			firstAskA = false;
		}
		console.afficheLigneSansRetour("Vous annoncez: ");
		try
		{
			console.demandeInput();
			annonce = Integer.parseInt(console.lastInput);
			endTimeA = System.currentTimeMillis();
			if (inTimeA(startTimeA, endTimeA))
				partie.traitementChoix(annonce);
			else
				connexionReseau.envoiErreurClient("horsDelaiA");
		} catch (NumberFormatException nfe)
		{
			System.out.println("Veuillez n'entrer que des nombres entiers > 0");
		}
	}

	private void demandePlaque()
	{
		console.affichePlaques(partie.getPlaquesdispo());
		console.afficheLigneSansRetour("Choisissez une plaque: ");
		try
		{
			Integer plaque = Integer.parseInt(console.demandeInput());
			partie.traitementChoix(plaque);
		} catch (NumberFormatException nfe)
		{
			nfe.printStackTrace();
		}
	}

	private void demandeOperation()
	{
		console.afficheLigneSimple("Choisissez une opération entre + - * et /");
		console.afficheLigneSansRetour("Votre choix: ");
		try
		{
			// Ne prend en compte que le premier char
			Character ope = (Character) console.demandeInput().charAt(0);
			partie.traitementChoix(ope);
		} catch (NumberFormatException nfe)
		{
			nfe.printStackTrace();
		}
	}

	private void demandeResultat()
	{
		console.afficheLigneSimple(partie.getStringOpeArithm("TrtResultat")
				+ "?");
		try
		{
			resultat = Integer.parseInt(console.demandeInput());
			endTimeE = System.currentTimeMillis();
			if (inTimeE(startTimeE, endTimeE))
				partie.traitementChoix(resultat);
			else
				connexionReseau.envoiErreurClient("horsDelaiE");
		} catch (NumberFormatException nfe)
		{
			nfe.printStackTrace();
		}
	}

	// methodes secondaires: ------------------------------------

	private boolean inTimeA(long startTimeA, long endTimeA)
	{ // TODO
		if ((endTimeA - startTimeA) <= deltaA)
			return true;
		return false;
	}

	private boolean inTimeE(long startTimeA, long endTimeA)
	{ // TODO
		if ((endTimeE - startTimeE) <= deltaE)
			return true;
		return false;
	}

	private void trtErreurFatale(String erreur)
	{
		console.afficheLigneSimple("GAME OVER erreur de type: " + erreur);
		connexionReseau.envoiErreurClient(erreur);
	}

	public void trtRetourServer(String erreur, Boolean getRespond,
			Boolean accordServer)
	{
		if (getRespond)
		{
			console.afficheLigneSimple(erreur + "prise en compte par le server");
			if (accordServer)
				ctrlJeu.demandeChoixMenu();
			else
			{
				if (erreur.compareTo("horsDelaiA") == 0)
					partie.traitementChoix(annonce);
				if (erreur.compareTo("horsDelaiE") == 0)
					partie.traitementChoix(resultat);
			}
		} else
		{
			console.afficheLigneSimple("Pas de retour server sur " + erreur);
			ctrlJeu.demandeChoixMenu();
		}
	}

	// Timer...Reste à les placer...et à prévoir le moment ou on les
	// arrêtent
	// (annonce acceptée,solution acceptée)!
	private void timerAnnonce()
	{// TODO
		// Affiche tous les x sec le temps restant
	}

	private void timerEncodage()
	{// TODO
		// Affiche tous les x sec le temps restant
	}
}
