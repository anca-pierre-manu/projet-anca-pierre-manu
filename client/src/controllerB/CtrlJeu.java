package controllerB;

import java.util.Scanner;

import vueB.VueConsole;
import ModeleB.ErreurOrMessage;

public class CtrlJeu
{
	private VueConsole console;
	private CtrlPartie ctrlPartie;
	private Scanner input;
	
	private String targetIP;
	private int targetPort;
	
	public CtrlJeu(String ip, int port)
	{
		input = new Scanner(System.in);
		input.useDelimiter("\n"); //\n <=> Insert a newline in the text at this point.
		
		this.targetIP = ip;
		this.targetPort = port;
		
		console = new VueConsole();
		demandeChoixMenu();
	}

	public void demandeChoixMenu()
	{
		int choix = -1;
		do{
			console.afficheMenu();
			try{
				choix = Integer.parseInt(console.demandeInput());
			}catch (NumberFormatException nfe) { /* Do Nothing */ }
		}
		while( verifChoixMenu(choix) );
	}

	private boolean verifChoixMenu(int choix)
	{
		switch(choix)
		{
			case 1:
				ctrlPartie = new CtrlPartie(this,console,targetIP,targetPort);
				return true;
			case 2:
				console.afficheLigneSansRetour("Au revoir! (appuyer sur n'importe quelle touche pour quitter).");
				input.nextLine();
				return false;
			default:
				console.afficheLignePasseLigne(ErreurOrMessage.eMenu);
				console.demandeInput();
				return true;
		}
	}
}
