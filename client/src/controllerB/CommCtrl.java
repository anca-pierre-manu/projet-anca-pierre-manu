package controllerB;

import ModeleB.Gestion_Partie;
import ModeleB.Operation_Arithmetique;
import connexion.ProtocoleClient;

import java.util.ArrayList;

public class CommCtrl implements GP_Observer
{
  private CtrlPartie ctrlPartie;
	private ProtocoleClient protocole;
	private Gestion_Partie partie;

	public CommCtrl(String ip, int port,CtrlPartie ctrlPartie,Gestion_Partie partie)
	{
    this.ctrlPartie =ctrlPartie;
		this.partie = partie;
    protocole = new ProtocoleClient(ip, port, this);
	}

	public void connectToServer()
	{
		protocole.connexionGameServer();
	}

	//Gestion des demandes du modèle :
	@Override
	public void update(String state, String subject, Object o)
	{
		try{
			if (state.compareTo("ClientToServer") == 0){
				if (subject.compareTo("NvJeu") == 0)
					protocole.trtDemandeNvJeu();
				if (subject.compareTo("Annonce") == 0)
					protocole.trtEnvoiAnnonce((Integer) o);
				if (subject.compareTo("Solution") == 0)
					protocole.trtEnvoiSolution((ArrayList<Operation_Arithmetique>) o);//DONE: implé une classe Op..Ar côté server pour verif la solution !
			}
		} catch (NullPointerException e)
		{
			System.out.println(e.getMessage());
		}
	}

  //appelé par CtrlPartie si erreur= "horsDelaiA","horsDelaiE","eMissPlaques","eResultat"
	public void envoiErreurClient(String erreur){
		protocole.trtEnvoiErreur(erreur);
	}

  //Réponse server sur: erreur= "horsDelaiA","horsDelaiE","eMissPlaques","eResultat" 
  //ET "ErreurSolution","ErreurDelaiAServer","ErreurDelaiEServer"
  public void reponseErreurClient(String erreur,boolean getRespond,boolean accordServer){
	  ctrlPartie.trtRetourServer(erreur, getRespond, accordServer);
  }

	public void reponseServerNvJeuOk(Integer lebut, ArrayList<Integer> plaquesDispo){
    partie.trtReponseServerNvJeu(lebut, plaquesDispo);
  }

	public void reponseServerAnnonceDelaiOk(){
    partie.trtReponseServerAnnonceDelaiOk();
  }

	public void reponseServerSolutionOk(int point){
    partie.trtReponseServerSolutionOk(point);
  }
  
}
