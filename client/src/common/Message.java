package common;

import java.io.Serializable;
import java.util.List;

import ModeleB.Operation_Arithmetique;

public class Message implements Serializable
{
	private static final long serialVersionUID = 1L;

	private Codes codeMessage 	    = Codes.MESSAGE_NOT_OK;
	private String 	nomMessage		= "";
	private Integer annonce			= 0;
	private Integer but             = 0;
	private List<Integer> plaques    = null;
	private List<Operation_Arithmetique> operations		= null;
	
	private boolean erreurAcceptee	= false;
	private String 	messageClient	= "";

	private Integer scoreFinalJoueur    = 0;

	private String paquetMessage	= "";
	private String dataDelimiter	= "::";

	private boolean isReadyToSend	= false;

	private Message(Codes codeMessage)
	{
		this.codeMessage = codeMessage;
	}

	// Constructeur pour l'envoi d'un message de notification au serveur
	public Message(Codes codeMessage, String nomMessage)
	{
		this(codeMessage);
		this.nomMessage 	= nomMessage;

		makePaquet();
	}

	// Constructeur pour l'envoi d'un message au serveur
	public Message(Codes codeMessage, String nomMessage, String messageClient)
	{
		this(codeMessage, nomMessage);
		this.messageClient = messageClient;
	}
	
	// Constructeur pour l'envoi d'une r�ponse � une erreur client
	public Message(Codes codeMessage, String nomMessage, boolean erreurAcceptee)
	{
		this(codeMessage, nomMessage);
		this.erreurAcceptee = erreurAcceptee;
	}
	
	// Constructeur pour l'envoi de l'annonce au serveur OU l'envoi du score final par le serveur vers le client.
	public Message(Codes codeMessage, String nomMessage, Integer annonceOrScoreFinal)
	{
		this(codeMessage, nomMessage);
		if(this.codeMessage == Codes.MESSAGE_SEND_ANNONCE)
			this.annonce = annonceOrScoreFinal;
		else if(this.codeMessage == Codes.MESSAGE_CLIENT_VICTORY || this.codeMessage == Codes.MESSAGE_CLIENT_DEFEAT)
			this.scoreFinalJoueur = annonceOrScoreFinal;
	}

	// Constructeur pour l'envoi de la solution au serveur 
	public Message(Codes codeMessage, String nomMessage, List<Operation_Arithmetique> operations)
	{
		this(codeMessage, nomMessage);
		this.operations = operations;
	}

	// Constructeur pour l'envoi du but et des plaques de départ au client
	public Message(Codes codeMessage, String nomMessage, List<Integer> plaques, Integer leBut)
	{
		this(codeMessage, nomMessage);
		this.plaques = plaques;
		this.but = leBut;
	}

	@Override
	public String toString()
	{
		return paquetMessage;
	}

	private void makePaquet()
	{
		paquetMessage = codeMessage + dataDelimiter + nomMessage + dataDelimiter + paquetMessage;
		isReadyToSend = true;
	}

	public Codes getCodeMessage() { return codeMessage; }
	public String getNomMessage() { return nomMessage; }
	public Integer getAnnonce() { return annonce; }
	public Integer getBut() { return but; }
	public List<Operation_Arithmetique> getOperations() { return operations; }
	public List<Integer> getPlaques() { return plaques; }
	public String getMessageClient() { return messageClient; }
	public Integer getScoreFinalJoueur() { return scoreFinalJoueur; }
	public boolean getErreurAcceptee() { return erreurAcceptee; }

	public boolean isReadyToSend() { return isReadyToSend; }
}
