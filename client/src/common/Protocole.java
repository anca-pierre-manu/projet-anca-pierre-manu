package common;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public abstract class Protocole
{
	protected Socket socket;
	protected ObjectOutputStream out;
	protected ObjectInputStream in;
	protected Message lastReceivedPacket;
	protected String inetAddress;
	protected int port;
	
	protected void write(String leMessage)
	{
		if(leMessage.length() == 0)
			return;
		
		try
		{
			out.writeObject(leMessage);
			out.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	protected void write(Message lePaquet)
	{
		if(lePaquet == null) return;

		try
		{
			out.writeObject(lePaquet);
			out.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
