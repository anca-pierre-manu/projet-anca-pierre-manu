package launcher;

import controllerB.CtrlJeu;

public class ClientLauncher
{
	private static CtrlJeu ctrlJeu;
	
	public static void main(String[] args)
	{
		String ip = "localhost";
		int port = 52337;
	
		switch(args.length)
		{
		case 1:
			ip = args[0];
			break;
		case 2:
			ip = args[0];
			try 
			{
				port = Integer.parseInt(args[1]);
			} catch (NumberFormatException nfe) { port = 52337; }
			break;
		}
		
		ctrlJeu = new CtrlJeu(ip, port);
	}
}
