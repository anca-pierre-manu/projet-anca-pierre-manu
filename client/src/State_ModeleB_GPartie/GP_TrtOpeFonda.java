
package State_ModeleB_GPartie;

import ModeleB.ErreurOrMessage;
import ModeleB.Gestion_Partie;
import ModeleB.Operation_Arithmetique;
import java.util.ArrayList;

/**
 *
 * @author Pierre
 */
class GP_TrtOpeFonda extends GP_State{
  
  private final String stateString =  "TrtOpeFonda";
  //private final String lastStateString = "TrtPlaqueUn";  //for IG only
  
  public GP_TrtOpeFonda(Gestion_Partie gp,Integer lebut,ArrayList<Integer> pD,
                        Operation_Arithmetique opAr){
    super(gp,lebut,pD,opAr);
    //this.addStateToListMemento("TrtOpeFonda",this,this.plaquesDispo); //for IG only
    //notifyObservers(this.stateString,"nc",null);  //for IG only
  }

  @Override
  public void traitementChoix(Object o) {
    Character c = (Character) o;
    if(opeArithm.verifChoixOperation(c)){
      this.gp.setMyState(new GP_TrtPlaqueDeux(gp,lebut,plaquesDispo,opeArithm));
      notifyObservers("TrtPlaqueDeux","nc",null); //for c only
    }
    else
      notifyObservers("TrtOpeFonda","eOperation",ErreurOrMessage.eOperation);
  }

  @Override
  public String getStateString() {
    return this.stateString;
  }

  @Override
  public void start() {
    throw new UnsupportedOperationException("Not supported yet."); 
  }
  
  @Override
  public void trtReponseServerNvJeu(Integer lebut, ArrayList<Integer> plaquesDispo) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

  @Override
  public void trtReponseServerAnnonceDelaiOk() {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

  @Override
  public void trtReponseServerSolutionOk(int point) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }
  
}


  