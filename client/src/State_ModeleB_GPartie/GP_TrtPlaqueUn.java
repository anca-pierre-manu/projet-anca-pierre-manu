
package State_ModeleB_GPartie;

import ModeleB.ErreurOrMessage;
import ModeleB.Gestion_Partie;
import ModeleB.Operation_Arithmetique;
import java.util.ArrayList;

 /**
 *
 * @author Pierre
 */
class GP_TrtPlaqueUn extends GP_State{
  
  private final String stateString = "TrtPlaqueUn";
  //private final String lastStateString = "TrtAnnonce";   //for IG only
  
  GP_TrtPlaqueUn(Gestion_Partie gp,Integer lebut,ArrayList<Integer> pD) {
    super(gp,lebut,pD);
    //this.addStateToListMemento("TrtPlaqueUn",this,this.plaquesDispo);  //for IG only
    //notifyObservers(this.stateString,"nc",null);  //for IG only
  }
  
  @Override
  public void traitementChoix(Object o) {
    this.setOpeArithm(new Operation_Arithmetique());
    Integer i = (Integer) o;
    if(verifChoixPlaque(i)){
      plaquesDispo.remove(i);
      //notifyObservers("TrtPlaqueUn","plaquesDispo",this.plaquesDispo); //En IG only !
      opeArithm.setPlaqueUn(i);
      gp.setMyState(new GP_TrtOpeFonda(gp,lebut,plaquesDispo,opeArithm));
      notifyObservers("TrtOpeFonda","nc",null); //for c only
      }
    else
      notifyObservers("TrtPlaqueUn","ePlaque",ErreurOrMessage.ePlaque);
  }

  @Override
  public void start() {
    throw new UnsupportedOperationException("Not supported yet."); 
  }
  
  @Override
  public String getStateString() {
    return this.stateString;
  }

  @Override
  public void trtReponseServerNvJeu(Integer lebut, ArrayList<Integer> plaquesDispo) {
    throw new UnsupportedOperationException("Not supported yet.");  
  }

  @Override
  public void trtReponseServerAnnonceDelaiOk() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void trtReponseServerSolutionOk(int point) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

}
