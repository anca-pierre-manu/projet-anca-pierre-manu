
package State_ModeleB_GPartie;

import ModeleB.ErreurOrMessage;
import ModeleB.Gestion_Partie;
import ModeleB.Operation_Arithmetique;
import java.util.ArrayList;

/**
 * 
 * @author Pierre
 */
class GP_TrtResultat extends GP_State{
  
  private final String stateString = "TrtResultat";
  //private final String lastStateString = "TrtPlaqueDeux"; //for IG only
  
  public GP_TrtResultat(Gestion_Partie gp,Integer lebut,ArrayList<Integer> pD,
                        Operation_Arithmetique opAr){
    super(gp,lebut,pD,opAr);
    //this.addStateToListMemento("TrtResultat",this,this.plaquesDispo); //for IG only
    //notifyObservers(this.stateString,"nc",null);  //for IG only
  }

  @Override
  public void traitementChoix(Object o) {
    Integer i = (Integer) o;
    if(this.opeArithm.verifChoixResultat(i)){
      this.solution.addListeOpeArithm(this.opeArithm);
      notifyObservers("TrtResultat","fullOpeAri",this.opeArithm.toString());
      lancementSuiteOuFin(i);
    } 
    else
      notifyObsForEresult(); 
  }
  
  private void notifyObsForEresult(){
    notifyObservers("TrtResultat","eResultat",ErreurOrMessage.eResultat);
    //notifyObservers("TrtResultat","end","nc");//For IG only
  }

  private void lancementSuiteOuFin(Integer i) {
    if(resultatEgalAnnonce(i))
      envoiSolutionClient();
    else{
      if(this.plaquesDispo.size()>0){
        this.plaquesDispo.add(i);
        iniToNextRound(plaquesDispo);
        //notifyObsForNextRound(); //For IG only
      }else
        notifyObsForMissPlaques();
    }
  }

  private void envoiSolutionClient(){
    notifyObservers("ClientToServer","Solution",this.solution.getListeOpeArithm());
  }
  
  @Override
  public void trtReponseServerSolutionOk(int point){
    this.point = point;
    notifyObsForWinMsg();  
  }
  
  private void notifyObsForWinMsg(){
    String msg = ErreurOrMessage.mWin +point+" points !";
    notifyObservers("TrtResultat","mWin",msg); 
    //notifyObservers("TrtResultat","end","nc");//for IG only
  }
  
  private boolean resultatEgalAnnonce(Integer i) {
    if((int)this.solution.getAnnonce()==(int)i)
      return true;
    else
      return false;
  }  
  
  private void iniToNextRound(ArrayList<Integer> pD) {
    //gp.saveRound();  //for IG only
    gp.setMyState(new GP_TrtPlaqueUn(gp,lebut,pD));
    notifyObservers("TrtPlaqueUn","nc",null);//for c only
  }
  
  private void notifyObsForMissPlaques(){
    notifyObservers("TrtResultat","eMissPlaques",ErreurOrMessage.eMissPlaques);
    //notifyObservers("TrtResultat","end","nc");//For IG only
  }
  
  @Override 
  public String getStateString() {
    return this.stateString;
  }

  @Override
  public void start() {
    throw new UnsupportedOperationException("Not supported yet."); 
  }
  
  @Override
  public void trtReponseServerNvJeu(Integer lebut, ArrayList<Integer> plaquesDispo) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

  @Override
  public void trtReponseServerAnnonceDelaiOk() {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

}
