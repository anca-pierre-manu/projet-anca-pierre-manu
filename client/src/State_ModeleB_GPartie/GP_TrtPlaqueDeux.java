
package State_ModeleB_GPartie;

import ModeleB.ErreurOrMessage;
import ModeleB.Gestion_Partie;
import ModeleB.Operation_Arithmetique;
import java.util.ArrayList;

/**
 *
 * @author Pierre
 */
class GP_TrtPlaqueDeux extends GP_State {

  private final String stateString = "TrtPlaqueDeux";
  //private final String lastStateString = "TrtOpeFonda";  //For IG only
  
  public GP_TrtPlaqueDeux(Gestion_Partie gp,Integer lebut,ArrayList<Integer> pD,
                          Operation_Arithmetique opAr){
    super(gp,lebut,pD,opAr);
    //this.addStateToListMemento("TrtPlaqueDeux",this,this.plaquesDispo);  //for IG only
    //notifyObservers(this.stateString,"nc",null);  //for IG only
  }
 
  @Override
  public void traitementChoix(Object o) {
    Integer i = (Integer) o;
    this.opeArithm.setPlaqueDeux(i);
    int j = opeArithm.giveGoodResultat();
    if(verifChoixPlaque(i) && j>=0){
      this.plaquesDispo.remove(i);
      notifyObservers("TrtPlaqueDeux","plaquesDispo",this.plaquesDispo);//en IG only
      this.opeArithm.setPlaqueDeux(i);
      this.gp.setMyState(new GP_TrtResultat(gp,lebut,plaquesDispo,opeArithm));
      notifyObservers("TrtResultat","nc",null);//for c only
      }
    else if (!verifChoixPlaque(i))
      this.notifyObservers("TrtPlaqueDeux","ePlaque",ErreurOrMessage.ePlaque);
    else{
      this.notifyObservers("TrtPlaqueDeux","eCalcul",ErreurOrMessage.getEcalcul(j));
    }
  }

  @Override
  public String getStateString() {
     return this.stateString;
  }

  @Override
  public void start() {
    throw new UnsupportedOperationException("Not supported yet."); 
  }
  
  @Override
  public void trtReponseServerNvJeu(Integer lebut, ArrayList<Integer> plaquesDispo) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

  @Override
  public void trtReponseServerAnnonceDelaiOk() {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

  @Override
  public void trtReponseServerSolutionOk(int point) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
}