
package State_ModeleB_GPartie;

import ModeleB.ErreurOrMessage;
import ModeleB.Gestion_Partie;
import java.util.ArrayList;

/**
 *
 * @author Pierre
 */
class GP_TrtAnnonce extends GP_State{
  
  private final String stateString = "TrtAnnonce";
  //private final String lastStateString = "TrtInitial";   //for IG only

  GP_TrtAnnonce(Gestion_Partie gp,Integer lebut,ArrayList<Integer> pD) {
    super(gp,lebut,pD);
    //addStateToListMemento("TrtAnnonce",this,this.plaquesDispo); //for IG only
    //notifyObservers("TrtAnnonce","nc",null); //for IG only
  }
  
  @Override
  public void traitementChoix(Object o) {
    if(solution.verifChoixAnnonce((Integer)o))
      envoiAnnonceClient();
    else
      notifyObservers("TrtAnnonce","eAnnonce",ErreurOrMessage.eAnnonce);
  }
  
  private void envoiAnnonceClient(){
    notifyObservers("ClientToServer","Annonce",this.solution.getAnnonce());
  }
  
  @Override
  public void trtReponseServerAnnonceDelaiOk(){
    notifyObservers("TrtAnnonce","annonce",this.solution.getAnnonce());
    gp.setMyState(new GP_TrtPlaqueUn(gp,lebut,plaquesDispo));
    notifyObservers("TrtPlaqueUn","nc",null); //for c only
  }
  
  @Override
  public void start() {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

  @Override
  public String getStateString() {
    return this.stateString;
  }

  @Override
  public void trtReponseServerNvJeu(Integer lebut, ArrayList<Integer> plaquesDispo) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

  @Override
  public void trtReponseServerSolutionOk(int point) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

}
