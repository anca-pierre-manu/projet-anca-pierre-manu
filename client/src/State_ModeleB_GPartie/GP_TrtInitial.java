
package State_ModeleB_GPartie;

import ModeleB.Gestion_Partie;
import java.util.ArrayList;

/**
 *
 * @author Pierre
 */
public class GP_TrtInitial extends GP_State{
  
  private final String stateString = "TrtInitial";
  //private final String lastStateString = null;    //for IG only

  public GP_TrtInitial(Gestion_Partie gp) {
    super(gp);
    initialisePartie();
    //addStateToListMemento("TrtInitial",this,this.plaquesDispo);  //for IG only
  }
  
  private void initialisePartie() {
    this.plaquesDispo = new ArrayList<>();
    this.plaquesDispo.clear();
  }
  
  @Override
  public void start(){
    demandeClientNvJeu();  
  }

  private void demandeClientNvJeu(){
    notifyObservers("ClientToServer","NvJeu",null);
  }
  
  @Override
  public void trtReponseServerNvJeu(Integer lebut,ArrayList<Integer> plaquesDispo){
    this.lebut = lebut;
    this.plaquesDispo = plaquesDispo;
    notifyObservers("TrtInitial","but",lebut);
    notifyObservers("TrtInitial","plaquesDispo",plaquesDispo);
    setMyState(new GP_TrtAnnonce(gp,lebut,plaquesDispo));
    notifyObservers("TrtAnnonce","nc",null); //for c only
  }

  @Override
  public void traitementChoix(Object o) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }

  @Override
  public String getStateString() {
    return stateString;
  }

  @Override
  public void trtReponseServerAnnonceDelaiOk() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void trtReponseServerSolutionOk(int point) {
    throw new UnsupportedOperationException("Not supported yet."); 
  }
 
}
