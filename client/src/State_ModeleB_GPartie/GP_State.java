
package State_ModeleB_GPartie;

import ModeleB.Gestion_Partie;
import ModeleB.Operation_Arithmetique;
import ModeleB.Solution;
import java.util.ArrayList;

/**
 *
 * @author Pierre
 */
public abstract class GP_State {
  
  Gestion_Partie gp;
  Solution solution;
  //GP_CareTaker gardien;  //for IG only
  
  Operation_Arithmetique opeArithm;
  ArrayList<Integer> plaquesDispo;
  Integer lebut;
  int point;

  public GP_State(Gestion_Partie gp) {
    this.gp = gp;
    solution = gp.getSolution();
    //gardien = gp.getGardien();   //for IG only
  }
 
  public GP_State(Gestion_Partie gp,Integer lebut,ArrayList<Integer> pD){
    this(gp);
    this.lebut =lebut;
    this.plaquesDispo =pD;
  }
  
  public GP_State(Gestion_Partie gp,Integer lebut,ArrayList<Integer> pD,
                  Operation_Arithmetique opAr){
    this(gp,lebut,pD);
    this.opeArithm = opAr;
  }
  
  public abstract void start();
  public abstract void traitementChoix(Object o);
  public abstract String getStateString();
  
  public abstract void trtReponseServerNvJeu(Integer lebut,
                                             ArrayList<Integer> plaquesDispo);
  public abstract void trtReponseServerAnnonceDelaiOk();
  public abstract void trtReponseServerSolutionOk(int point);
  
  
  public void setMyState(GP_State state) {
    this.gp.setMyState(state);
  }
  
  public GP_State getMyState() {
    return gp.getMyState();
  }
  
  public ArrayList<Integer> getPlaquesdispo(){
    return this.plaquesDispo;
  }
  
  
  public void setPlaquesDispo(ArrayList<Integer> oldPlaques){
    plaquesDispo = oldPlaques;                   
    //notifyObservers("all","plaquesDispo",plaquesDispo); //for IG only
  }
  
  public boolean verifChoixPlaque(Integer i){
    if(plaquesDispo.contains(i))
      return true;
    else
      return false;
  }
  
  public void notifyObservers (String state,String subject,Object o){
    gp.notifyObservers(state,subject,o);
  }

  public String getStringOpeArithm(String stateString) {
    if(opeArithm!=null)
      return opeArithm.partialString(stateString);
    else
      return "";
  }

  /**
   * @param opeArithm the opeArithm to set
   */
  public void setOpeArithm(Operation_Arithmetique opeArithm) {
    this.opeArithm = opeArithm;
  }
    
  
}
