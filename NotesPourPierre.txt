1) Dans le diagramme de classes final (dernière page du rapport 2eme iteration), ne peut-on pas changer l'interface 
		serveur en classe abstraite? On en aurait besoin car certaines méthodes auraient besoin d'être "protected". -> ok

2) Utilité du ServerFactory? -> pattern singleton, ok

3) Classe "Message" renommée en "Packets"

3b) Les "Message"s sont en fait des packets, qui seront les objets que nous enverrons sur le réseau. Le client et le serveur sauront lire ces objets et déduire leur type.
		Par exemple, le serveur envoie un packet ConnAckPacket (un ACK) au client. Celui-ci lira l'objet depuis le réseau, et pourra caster son type pour pouvoir lire ce que le paquet contient.
		Il faut que la classe Packet et ses enfants puissent implémenter l'interface Serializable, afin qu'à chaque envoi, les objets soient convertis en binaire et envoyé sur le réseau.
		
		Les types de packets qu'on a pour l'instant sont:
			- la classe abstraite Packet, qui est la classe mère à tous les Packet
			- ConnAckPacket (un paquet ACK)
			- ConnAskPacket (le paquet que le client envoie au serveur pour demander (ask) la connection tcp initiale)
			- ConnTerminatePacket (le paquet envoyé en cas de déconnection par le client OU le serveur (ça marche dans les 2 sens)).
		
		Autres packets à prévoir :
			- ConnSendGamePacket (le paquet qui enverra les plaques et le but au client)
			- ConnSendSolutionPacket (le paquet envoyé par le client au serveur lorsque celui-ci à terminé)
			- ConnTimeoutPacket (ce paquet aura 2 utilités : 1) envoyé par le client si il met trop longtemps pour l'annonce du but
															 2) envoyé par le serveur au client si il a mis trop de temps à donner sa solution)
			- D'autres paquets à prévoir?