package model;

import java.net.ServerSocket;

public abstract class AServer
{
	protected int port;
	protected ServerSocket servSocket;
	protected boolean isServerRunning;
	
	public AServer()
	{
		port = 52337;
		servSocket = null;
		isServerRunning = false;
	}
	
	public AServer(int port)
	{
		this();
		this.port = port;
	}
	
	/**
	 * La boucle principale du serveur
	 */
	protected abstract void serverLoop();

	/**
	 * Méthode qui crée le servSocket et démarre le serveur sur le port spécifié à la construction du serveur.
	 */
	public abstract void startServer();

	/**
	 * Déconnecte tous les clients, stoppe le serveur et détruit la(le?) servSocket.
	 */
	public abstract void stopServer();
	
	public abstract boolean isServerRunning();
}
