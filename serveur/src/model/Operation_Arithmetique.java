/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 * @author Pierre
 */
public class Operation_Arithmetique implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private int plaqueUn = -1;
	private int plaqueDeux = -1;
	private char opeFonda = ' ';
	private int resultat = -1;

	public boolean verifChoixOperation(char c)
	{
		if (c == '/' || c == '*' || c == '-' || c == '+')
		{
			this.opeFonda = c;
			return true;
		} else
			return false;
	}

	public boolean verifChoixResultat(int i)
	{
		if (i == giveGoodResultat() && i != -1)
		{
			this.resultat = i;
			return true;
		} else
			return false;
	}

	public int giveGoodResultat()
	{
		int nb1 = this.getPlaqueUn();
		int nb2 = this.getPlaqueDeux();
		
		int goodResult = 1;
		
		switch (this.getOpeFonda())
		{
			case '+':
				goodResult = nb1 + nb2;
				if(goodResult < 0)
					goodResult = -1;
				break;
			case '-':
				goodResult = nb1 - nb2;
				if(goodResult < 0)
					goodResult = -1;
				break;
			case '/':
                try
                {
	                if (nb1%nb2 != 0 || nb2==0)
		                goodResult = -1;
	                else
		                goodResult = nb1 / nb2;
                } catch (ArithmeticException ae)
                {
	                goodResult = -1;
                }
				break;
			case '*':
				goodResult = nb1 * nb2;
				if(nb1 <= 0 || nb2 <= 0)
					goodResult = -1;
				break;
		}
    
		return goodResult;
	}
  
	@Override
	public String toString()
	{
		return "" + plaqueUn + " " + opeFonda + " " + plaqueDeux + " = " + resultat + "";
	}

	/**
	 * @param plaqueUn the plaqueUn to set
	 */
	public void setPlaqueUn(int plaqueUn)
	{
		this.plaqueUn = plaqueUn;
	}

	/**
	 * @param plaqueDeux the plaqueDeux to set
	 */
	public void setPlaqueDeux(int plaqueDeux)
	{
		this.plaqueDeux = plaqueDeux;
	}

	/**
	 * @return the plaqueUn
	 */
	public int getPlaqueUn()
	{
		return plaqueUn;
	}

	/**
	 * @return the plaqueDeux
	 */
	public int getPlaqueDeux()
	{
		return plaqueDeux;
	}

	/**
	 * @return the opeFonda
	 */
	public char getOpeFonda()
	{
		return opeFonda;
	}

	/**
	 * @return the resultat
	 */
	public int getResultat()
	{
		return resultat;
	}

}
