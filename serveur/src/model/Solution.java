/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Pierre
 */
public class Solution implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private int annonce = -1;
	private ArrayList<Operation_Arithmetique> listeOpeArithm = new ArrayList<>();

	public boolean verifChoixAnnonce(int i)
	{
		if ((i >= 100) && (i <= 999))
		{
			this.annonce = i;
			return true;
		} else
			return false;
	}

	public void addListeOpeArithm(Operation_Arithmetique opeArithm)
	{
		listeOpeArithm.add(opeArithm);
	}

	public ArrayList<String> listeSolution()
	{
		ArrayList<String> li = new ArrayList<>();
		for (Operation_Arithmetique opAr : listeOpeArithm)
			li.add(opAr.toString());
		return li;
	}

	/**
	 * @return the annonce
	 */
	public int getAnnonce()
	{
		return annonce;
	}


}