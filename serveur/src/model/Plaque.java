/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Pierre
 */
public class Plaque implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Integer> domaineMetierPlaque;

	public Plaque()
	{
		initialiseDomaineMetierPlaque();
	}

	private void initialiseDomaineMetierPlaque()
	{
		this.domaineMetierPlaque = new ArrayList();
		int tour = 1;
		while (tour <= 2)
		{
			for (int i = 1; i <= 10; ++i)
			{
				domaineMetierPlaque.add(i);
			}
			++tour;
		}
		for (int j = 1; j <= 4; ++j)
		{
			domaineMetierPlaque.add(j * 25);
		}
	}

	public Integer giveOutAndRemoveInPlaque()
	{   // a placer dans un try catch mais bloque à cause de return !
		int index = (int) (Math.random() * (domaineMetierPlaque.size() - 1));
		return (Integer) this.domaineMetierPlaque.remove(index);
	}

}
