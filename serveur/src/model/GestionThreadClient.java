package model;

import java.util.ArrayList;
import java.util.List;

import common.Codes;
import common.EtatClient;
import common.Message;
import connexion.ProtocoleServer;

public class GestionThreadClient implements Runnable
{	
	private ClientInfo clientInfo;
	private boolean isAlive;
	private EtatClient etatClient;
	private static final long MAX_ANNONCE_TIME = 10000; // Millisecondes
	private static final long MAX_ENCODAGE_TIME = 60000; // Millisecondes
	
	public GestionThreadClient(ClientInfo clInfo)
	{
		this.clientInfo = clInfo; // supprimer la classe clientinfo
		this.isAlive = true;
		etatClient = EtatClient.WAIT_ANNONCE;
	}
	
	@Override
	public void run()
	{
		// reponse par un ACK � la demande de connection du client
		getProtocole().envoiAck();
		etatClient = EtatClient.CONNECTED;
		
		while(isAlive)
		{
			// Attendre message du client
			try
			{
				Message msg = getProtocole().attendreMessage();
				traiterMessageRecu(msg);
			} 
			catch(NullPointerException npe)
			{
				System.out.println("Le client s'est déconnecté, arrêt du thread.");
				stop("Le client s'est déconnecté.");
			}
		}
	}

	private void traiterMessageRecu(Message message)
	{
		  System.out.println("Recu un message de type " + message.getCodeMessage().toString());
    switch(message.getCodeMessage())
		{
			case MESSAGE_ASK_NEWGAME:
        creerNouveauJeu();
				getProtocole().trtNouveauJeuClient(clientInfo);
				break;
			
			case MESSAGE_SEND_ANNONCE:
				boolean timerRespecte = checkDeltaTempsAnnonce(System.currentTimeMillis(), 
						clientInfo.getTempsDebutAnnonce()); 
				Integer annonce = message.getAnnonce();
				
				if(timerRespecte)
				{
					endAnnonceClient(annonce);
					startEncodageClient();
				}
				getProtocole().trtReceptionAnnonceClient(timerRespecte);
				
				
				break;
			
			case MESSAGE_SEND_SOLUTION:
				List<Operation_Arithmetique> operations = message.getOperations();
				boolean respectTimer = checkDeltaTempsEncodage(System.currentTimeMillis(), clientInfo.getTempsDebutEncodage());
				Integer scoreFinal = calculerPointsSolutionFinale(operations);
				getProtocole().trtReceptionSolutionClient(respectTimer, scoreFinal);
				break;
			
			case MESSAGE_SEND_ERROR:
				Codes codeErreur = message.getCodeMessage();
				String messageDerreur = message.getMessageClient();
				boolean erreurAcceptee = traitementErreurClient(codeErreur);
				
				getProtocole().trtReceptionErreurClient(erreurAcceptee, messageDerreur);
				
				if(erreurAcceptee)
					stop("Arret du thread et fermeture du socket pour l'erreur suivante : " + messageDerreur);
				
				break;
			
			case MESSAGE_DISCONNECT:
				this.stop("Le client s'est déconnecté du serveur.");
				break;
			
			default:
				break;
		}
	}
	
	// Retourne true si le serveur accepte l'erreur, sinon false;
	private boolean traitementErreurClient(Codes codeErreur) {
		switch(codeErreur)
		{
		case MESSAGE_HORS_DELAI_A:
			if( ! checkDeltaTempsAnnonce(System.currentTimeMillis(), clientInfo.getTempsDebutAnnonce()))
				return true;
			break;
			
		case MESSAGE_HORS_DELAI_E:
			if( ! checkDeltaTempsEncodage(System.currentTimeMillis(), clientInfo.getTempsDebutEncodage()))
				return true;
			break;
			
		case MESSAGE_ERESULTAT:
			return true;
			
		case MESSAGE_EMISSPLAQUE:
			return true;
			
		default:
			return false;
		}
		
		return false;
	}

	public void stop(String reason)
	{
		getProtocole().disconnect(reason);
		isAlive = false;
	}

	public String getClientIP()
	{
		return clientInfo.getClientIP();
	}

	public ProtocoleServer getProtocole()
	{
		return clientInfo.getProtocole();
	}

	private void creerNouveauJeu()
	{
		Integer leBut = new But().genereBut();
		Plaque p = new Plaque();
		ArrayList<Integer> plaques = new ArrayList<>();
		for(int i = 0 ; i < 6 ; ++i)
			plaques.add(p.giveOutAndRemoveInPlaque());
		clientInfo.setBut(leBut);
		clientInfo.setPlaques(plaques);
		
		// Set temps d'annonce debut
		clientInfo.resetTempsDebutAnnonce(System.currentTimeMillis());
	}
	
	private boolean checkDeltaTempsAnnonce(long timeEndAnnonce, long timeStartAnnonce)
	{
		if((timeEndAnnonce - timeStartAnnonce) <= MAX_ANNONCE_TIME)
			return true;
		return false;
	}
	
	private void endAnnonceClient(Integer valeur)
	{
		clientInfo.setAnnonceClient(valeur);
	}
	
	private void startEncodageClient()
	{
		etatClient = EtatClient.WAIT_ENCODAGE;
		clientInfo.resetTempsDebutEncodage(System.currentTimeMillis());
	}
	
	private boolean checkDeltaTempsEncodage(long timeEndEncodage, long timeStartEncodage)
	{
		if((timeEndEncodage-timeStartEncodage) < MAX_ENCODAGE_TIME)
			return true;
		return false;
	}
	
	private Integer calculerPointsSolutionFinale(List<Operation_Arithmetique> operations)
	{
		Integer score = 0;
		
		etatClient = EtatClient.ENDGAME;
		return score;
	}
}
