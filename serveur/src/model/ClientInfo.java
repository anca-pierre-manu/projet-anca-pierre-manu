package model;

import connexion.ProtocoleServer;

import java.util.ArrayList;
import java.util.List;

public class ClientInfo
{
	private String clientIP;
	private ProtocoleServer protocole;
	
	private Integer but;
	private List<Integer> plaques;
	private Integer annonceClient;
	
	private long tempsDebutAnnonce;
	private long tempsDebutEncodage;

	public ClientInfo(String clientIP, ProtocoleServer protocole)
	{
		this.clientIP = clientIP;
		this.protocole = protocole;
		
		this.annonceClient = -1;
		this.tempsDebutAnnonce = -1l;
		this.tempsDebutEncodage = -1l;
		
		but = 0;
		plaques = new ArrayList<>();
	}

	public void resetTempsDebutAnnonce(long tempsDebut)
	{
		this.tempsDebutAnnonce = tempsDebut;
	}
	
	public long getTempsDebutAnnonce()
	{
		return this.tempsDebutAnnonce;
	}
	
	public void resetTempsDebutEncodage(long tempsDebutEncodage)
	{
		this.tempsDebutEncodage = tempsDebutEncodage;
	}
	
	public long getTempsDebutEncodage()
	{
		return this.tempsDebutEncodage;
	}
	
	public void setBut(Integer but)
	{
		this.but = but;
	}

	public Integer getBut() { return this.but; }

	public void setPlaques(List<Integer> plaques)
	{
		this.plaques = plaques;
	}

	public List<Integer> getPlaques() { return this.plaques; }

	public void setAnnonceClient(Integer annonce)
	{
		this.annonceClient = annonce;
	}

	public Integer getAnnonceClient()
	{
		return annonceClient;
	}

	public String getClientIP()
	{
		return clientIP;
	}

	public ProtocoleServer getProtocole()
	{
		return protocole;
	}
}
