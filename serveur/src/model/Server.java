package model;

import connexion.ProtocoleServer;
import controller.ClientCtrl;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends AServer
{
	private ClientCtrl clientThreadsController;
	
	public Server()
	{
		super();
		clientThreadsController = new ClientCtrl();
	}
	
	public Server(int port)
	{
		super(port);
		clientThreadsController = new ClientCtrl();
	}
	
	@Override
	public void serverLoop()
	{
		try
		{
			
			System.out.println("Le serveur accepte les connections entrantes sur le port " + port);
			
			while(isServerRunning)
			{
				Socket s = servSocket.accept(); // Bloquant
				System.out.println("Connexion client detectee. (IP source: " + s.getInetAddress().toString() + ")");
				ProtocoleServer ps = new ProtocoleServer(s);
				clientThreadsController.createNewClientThread(ps);
			}
		}
		catch (IOException ioe)
		{
			System.out.println("Error: " + ioe.getMessage());
		}
	}

	@Override
	public void startServer()
	{
		try
		{
			servSocket = new ServerSocket(port);
			
			isServerRunning = true;
			serverLoop();
		}
		catch (IOException e)
		{
			System.out.println("Ne peut créer le servSocket du serveur sur le port " + port + "! (Est-il déjà en cours " +
					"d'utilisation?)");
			System.exit(-1);
		}
	}

	@Override
	public void stopServer()
	{
		System.out.println("Arrêt serveur...");
		
		// 1) arrêt serveur
		isServerRunning = false;
		
		// 2) deconnection clients
		clientThreadsController.killAllConnectionsAndThreads();
		
		// 3) arrêt servSocket
		try
		{
			servSocket.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean isServerRunning()
	{
		return isServerRunning;
	}
}
