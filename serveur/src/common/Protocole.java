package common;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public abstract class Protocole
{
	protected Socket socket;
	protected ObjectOutputStream out;
	protected ObjectInputStream in;
	protected Message lastReceivedPacket;
	protected String inetAddress;
	protected int port;
	
	public Protocole()
	{
		socket = null;
		out = null;
		in = null;
		lastReceivedPacket = null;
		inetAddress = "";
		port = -1;
	}
	
	protected void write(Message lePaquet)
	{
		if(lePaquet == null) return;

		try
		{
			out.writeObject(lePaquet);
			out.flush();
		} catch (IOException e)
		{
			System.out.println("Le socket est fermé et ne peut donc plus etre utilise.");
		}
	}
}
