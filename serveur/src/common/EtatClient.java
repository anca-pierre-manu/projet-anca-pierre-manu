package common;

public enum EtatClient
{
	CONNECTED,
	WAIT_ANNONCE,
	WAIT_ENCODAGE,
	ENDGAME,
}
