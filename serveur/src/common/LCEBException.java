package common;

public class LCEBException extends Exception
{
	private String message;
	
	public LCEBException(String errorMessage)
	{
		this.message = errorMessage;
	}

	@Override
	public String getMessage()
	{
		return message;
	}
}
