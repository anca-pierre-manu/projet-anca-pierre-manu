package common;

public enum Codes {
	CONN_OK,
	CONN_KO,
	CONN_NOT_LCEB,

	MESSAGE_ACK,
	MESSAGE_OK,
	MESSAGE_NOT_OK,
	MESSAGE_TIMEOUT,
	MESSAGE_DISCONNECT,

	MESSAGE_ASK_CONNECTION,
	MESSAGE_ASK_NEWGAME,
	MESSAGE_SEND_NEWGAME,

	MESSAGE_SEND_ANNONCE,
	MESSAGE_ANNONCE_OK,

	MESSAGE_SEND_SOLUTION,
	
	MESSAGE_HORS_DELAI_A,
	MESSAGE_HORS_DELAI_E,
	MESSAGE_ERESULTAT,
	MESSAGE_EMISSPLAQUE,

	MESSAGE_CLIENT_VICTORY,
	MESSAGE_CLIENT_DEFEAT,
	MESSAGE_SEND_ERROR,
	MESSAGE_SEND_MSG,
	
	MESSAGE_SEND_ERROR_ANSWER,
}