package connexion;

import common.Codes;
import common.Message;
import common.Protocole;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

import model.ClientInfo;
import model.Operation_Arithmetique;

public class ProtocoleServer extends Protocole
{
	public ProtocoleServer(Socket s)
	{
		socket = s;
		try
		{
			out = new ObjectOutputStream(s.getOutputStream());
			in = new ObjectInputStream(s.getInputStream());
			inetAddress = s.getInetAddress().toString();
			port = s.getPort();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public String getClientIP()
	{
		return socket.getInetAddress().toString();
	}

	public void disconnect(String reason)
	{
		Message terminate = new Message(Codes.MESSAGE_DISCONNECT, "Deconnection: " + reason);

		try
		{
			if (socket.isConnected())
				write(terminate);
			socket.close();
		} catch (IOException e)
		{
		}
	}

	public Codes connexionClient() // inutilisé
	{
		try
		{
			Message ask = (Message) in.readObject();
			if (ask instanceof Message)
			{
				envoiAck();
				return Codes.MESSAGE_ACK;
			} else
				return Codes.CONN_NOT_LCEB;

		} catch (ClassNotFoundException | IOException e)
		{
			return Codes.CONN_KO;
		}
	}

	public Message attendreMessage()
	{
		Message reception = null;

		try
		{
			reception = (Message) in.readObject();
		} catch (ClassNotFoundException | IOException e)
		{
			return null;
		}

		return reception;
	}

	public Codes envoiPaquetAuClient(Message answer)
	{
		if (answer == null)
			return Codes.MESSAGE_NOT_OK;

		write(answer);

		try
		{
			Message ack = (Message) in.readObject();
			if (ack instanceof Message)
				return Codes.MESSAGE_OK;
		} catch (ClassNotFoundException | IOException e)
		{
			return Codes.MESSAGE_NOT_OK;
		}
		return Codes.MESSAGE_OK;
	}

	public Codes envoiPaquetAuClientSansAck(Message answer)
	{
		if (answer == null)
			return Codes.MESSAGE_NOT_OK;
		write(answer);
		return Codes.MESSAGE_OK;
	}

	public void envoiAck()
	{
		envoiPaquetAuClientSansAck(new Message(Codes.MESSAGE_ACK, "Ack"));
	}

	private boolean attendreAck()
	{
		Message msg = attendreMessage();
		if (msg.getCodeMessage() == Codes.MESSAGE_ACK)
			return true;
		return false;
	}

	// ------------------ Envois de messages d'erreurs --------------------

	public void envoiMessageErreurAnnonceTimeout()
	{
		Message erreur = new Message(Codes.MESSAGE_HORS_DELAI_A,
				"Timeout annonce");
		envoiPaquetAuClient(erreur);
	}

	// ------------------ Contrepartie operations client ------------------

	public void trtNouveauJeuClient(ClientInfo clInfo)
	{
		Message nouveauJeu = new Message(Codes.MESSAGE_SEND_NEWGAME,
				"Envoi nouveau jeu", clInfo.getPlaques(), clInfo.getBut());

		envoiPaquetAuClient(nouveauJeu);
	}

	public void trtReceptionAnnonceClient(boolean timerRespecte)
	{
		envoiAck();
		Message reponse = null; // Va dépendre des paramètres.

		if (timerRespecte)
			reponse = new Message(Codes.MESSAGE_ANNONCE_OK, "Annonce OK");
		else
			reponse = new Message(Codes.MESSAGE_HORS_DELAI_A, "Annonce timeout");

		envoiPaquetAuClient(reponse);
	}

	public void trtReceptionSolutionClient(boolean timerRespecte,
			Integer scoreFinalJoueur)
	{
		Message aEnvoyer = null; // Va dépendre des paramètres.

		if (!timerRespecte)
			aEnvoyer = new Message(Codes.MESSAGE_HORS_DELAI_E,
					"Solution timeout");
		else if (timerRespecte && scoreFinalJoueur <= 0)
			aEnvoyer = new Message(Codes.MESSAGE_CLIENT_DEFEAT, "Joueur perd",
					new Integer(0));
		else
			aEnvoyer = new Message(Codes.MESSAGE_CLIENT_VICTORY,
					"Joueur gagne", scoreFinalJoueur);

		envoiPaquetAuClient(aEnvoyer);
	}

	/**
	 * 
	 * @param erreurAcceptee
	 * @param codeErreur
	 * @param messageDerreur
	 * @return Si le client peut continuer � exister, sinon on stoppe le thread
	 *         et on d�truit le socket.
	 */
	public void trtReceptionErreurClient(boolean erreurAcceptee,
			String messageDerreur)
	{
		// envoiAck();

		System.out.println("Message d'erreur recu du client: " + getClientIP()
				+ " : " + messageDerreur);

		Message retourErreur = null;

		if (erreurAcceptee)
		{
			retourErreur = new Message(Codes.MESSAGE_SEND_ERROR_ANSWER,
					"Erreur Client confirmee", true);
		} else
		{
			retourErreur = new Message(Codes.MESSAGE_SEND_ERROR_ANSWER,
					"Erreur Client non confirmee", false);
		}

		envoiPaquetAuClientSansAck(retourErreur);

		// On attend pas particulièrement d'ACK dans ce cas-ci, surtout si le
		// client a planté.
	}
}