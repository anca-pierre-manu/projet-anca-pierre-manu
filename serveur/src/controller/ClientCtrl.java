package controller;

import connexion.ProtocoleServer;
import model.GestionThreadClient;
import model.ClientInfo;

import java.util.ArrayList;
import java.util.List;

public class ClientCtrl
{
	private static long client_id_giver = 0L;
	
	private List<GestionThreadClient> connectedClients;
	
	public ClientCtrl()
	{
		connectedClients = new ArrayList<>();
	}
	
	public void createNewClientThread(ProtocoleServer protocole)
	{
		String clientIP = protocole.getClientIP();
		
		ClientInfo clinfo = new ClientInfo(clientIP, protocole);
		GestionThreadClient thr = new GestionThreadClient(clinfo);
		Thread threadClient = new Thread(thr);
		connectedClients.add(thr);
		threadClient.start();
	}
	
	public void disconnectAndKillClientThreadByID(String ipToKill)
	{
		for(GestionThreadClient clthr : connectedClients)
		{
			if(clthr.getClientIP().equals(ipToKill))
				clthr.stop("Connection terminée.");
		}
	}
	
	public void killAllConnectionsAndThreads()
	{
		for(GestionThreadClient clthr : connectedClients)
		{
			clthr.stop("Fermeture du serveur.");
		}
	}
}
