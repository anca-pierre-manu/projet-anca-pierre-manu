package controller;

import model.AServer;
import model.Server;

public class ServerCtrl
{
	private AServer server;
	
	public ServerCtrl()
	{	
	}
	
	public void startServer(int port)
	{
		server = new Server(port);
		server.startServer();
	}
	
	public void stopServer()
	{
		server.stopServer();
	}
}
