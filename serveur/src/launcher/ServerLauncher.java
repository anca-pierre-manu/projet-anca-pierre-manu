package launcher;

import controller.ServerCtrl;

import java.util.Scanner;

public class ServerLauncher
{
	private int port;
	private ServerCtrl serverctrl;
	
	public ServerLauncher(String [] args)
	{
		port = 52337;
		
		if(args.length > 0)
		{
			try
			{
				port = Integer.parseInt(args[0]);
			} catch (NumberFormatException nbe) {}
		}
		
		serverctrl = new ServerCtrl();
		serverctrl.startServer(port);
	}
	
	public static void main(String []args)
	{
		new ServerLauncher(args);
	}
}
