import model.Server;

public class ServerTest
{
	@org.junit.Test
	public void testServerStart() throws Exception
	{
		final Server server = new Server(11111);
		new Thread(new ThreadTestServeur(server)).start();
		server.startServer();
	}
	
	// ************************* CLASSES INTERNES *************************
	
	private class ThreadTestServeur implements Runnable
	{
		private Server serveur;
		private final float TIMEOUT = 5000f;
		private float time = 0f;
		
		public ThreadTestServeur(Server serveur)
		{
			this.serveur = serveur;
		}
		
		@Override
		public void run()
		{
			while(time < TIMEOUT)
			{
				try
				{
					Thread.sleep(1000);
					time += 1000;
					System.out.println("Fermeture du serveur dans " + ((TIMEOUT-time)/1000) + " secondes.");
				} catch (InterruptedException ie) {}
			}
			serveur.stopServer();
		}
	}
}
